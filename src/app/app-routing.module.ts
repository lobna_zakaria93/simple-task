import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddItemComponent } from './pages/add-item/add-item.component';
import { ItemDetailsComponent } from './pages/item-details/item-details.component';
import { ItemListComponent } from './pages/item-list/item-list.component';



const routes: Routes = [
  {
    path:'item-list',
    component:ItemListComponent
  },
  {
    path:'item/:id/details',
    component:ItemDetailsComponent
  },
  {
    path:'item/add-new-item',
    component:AddItemComponent
  },
  {
    path: '',
    redirectTo: 'item-list',
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: 'item-list',
    pathMatch: 'full'
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
