import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'simple-task';
  opened: boolean= false;

  toggledMenu(e:any){
    console.log(e)
    this.opened = e;
  }
}
