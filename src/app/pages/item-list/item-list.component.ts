import { Component, OnInit, ViewChild } from "@angular/core";
import { MatTableDataSource } from "@angular/material/table";
import { ItemsService } from "src/app/services/items.service";
import {MatPaginator} from '@angular/material/paginator';
import { MatSort } from "@angular/material/sort";
import { Router } from "@angular/router";
@Component({
    templateUrl:'item-list.component.html',
    styleUrls:['item-list.component.scss']
})
export class ItemListComponent implements OnInit{
    displayedColumns: string[] = ['id','category', 'title',  'publishedAt'];
    dataSource:any = [];
    categories:any = [];

    @ViewChild(MatPaginator)
    paginator!: MatPaginator;
    @ViewChild(MatSort)
     sort! : MatSort;
    constructor(
        private service:ItemsService,
        private router:Router,
    ){}
    ngOnInit(){
        this.getItems();
        
    }
    getItems(){
        this.service.getJSON().subscribe(data => {
            this.dataSource = new MatTableDataSource(data.articles);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
            this.categories = data.sourceCategory;
        })
    }
    applyFilter(event: Event) {
        const filterValue = (event.target as HTMLInputElement).value;
        this.dataSource.filter = filterValue.trim().toLowerCase();
        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }
    onItemRowClicked(id:any){
        console.log(id);
        this.router.navigateByUrl(`item/${id}/details`);
    }
    onAddItemClicked(){
        this.router.navigateByUrl('item/add-new-item')
    }
}