import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ItemsService } from "src/app/services/items.service";

@Component({
    templateUrl:'item-details.component.html',
    styleUrls:['item-details.component.scss']
})
export class ItemDetailsComponent implements OnInit{
    itemDetails:any;
    itemID:any;
    categories:any = [];
    constructor(
        private service:ItemsService,
        private route:ActivatedRoute,
        private router:Router
    ){
        this.itemID = this.route.snapshot.params.id;
    }
    ngOnInit(){
        this.getItem();
    }
    getItem(){
        this.service.getJSON().subscribe(data => {
            this.categories = data.sourceCategory;
            data.articles.forEach((item:any) => {
                if(item.id == this.itemID){
                    this.itemDetails = item;
                }
            });
            console.log(this.itemDetails)
        })
    }
    backToList(){
        this.router.navigateByUrl('item-list');
    }
}