import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";
import { Router } from "@angular/router";
import { ItemsService } from "src/app/services/items.service";

@Component({
    templateUrl:'./add-item.component.html',
    styleUrls:['./add-item.component.scss']
})
export class AddItemComponent implements OnInit {

    itemForm!:FormGroup;
    categories:any = [];
    constructor(
        private router:Router,
        private service:ItemsService,
        private fb: FormBuilder
    ){}
    ngOnInit(){
        this.getCategories();
    }

    initItemForm(){
        this.itemForm = this.fb.group({
            title:[''],
            category:[''],
            description:['']
        })
    }
    getCategories(){
        this.service.getJSON().subscribe((data:any) => {
            this.categories = data.sourceCategory;
        })
    }
    backToList(){
        this.router.navigateByUrl('item-list');
    }
    SubmitItem(){
        console.log(this.itemForm.value);
        this.router.navigateByUrl('item-list');
    }
}