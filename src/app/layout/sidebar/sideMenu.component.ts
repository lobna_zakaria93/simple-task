import { Component, Input } from "@angular/core";

@Component({
    selector:'app-sideMenu',
    templateUrl:'sideMenu.component.html',
    styleUrls:['sideMenu.component.scss']
})
export class SideMenuComponent {
    @Input()opened: boolean= false;
}