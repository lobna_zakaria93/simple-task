import { Component, EventEmitter, OnInit, Output } from "@angular/core";

@Component({
    templateUrl:'./navbar.component.html',
    styleUrls:['navbar.component.scss'],
    selector:'app-navbar'
})
export class NavbarComponent implements OnInit{
    isSearch = false;
    opened = false;
    @Output()
    toggle: EventEmitter<boolean> = new EventEmitter<boolean>();
    ngOnInit(){

    }
    toggleMenu(){
        this.opened = !this.opened;
        this.toggle.emit(this.opened)
    }
}